--------------------------------------------
--    gmcl_proxi_win64.dll Feature List   --
--------------------------------------------

------------------- Hooks ------------------

PreFrameStageNotify(m_iStageNumber)							-				Used to get information about entities as it's networked
PostFrameStageNotify(m_iStageNumber)						-				Used to get information about entities after it's networked and applied
CreateMoveEx(CUserCmd, m_bSendPacket)						-				Called after CreateMove in CL_Move (Only runs when the tick is sent to the server. `return false, false` to choke a packet, can choke up to 21 packets)
DispatchEffect(m_strEffectName, m_pEffectData)				-				Called whenever decals or effects are made (util.Effect or engine)
OutgoingNetMessage(proxi_bf_read)							-				Called when net.SendToServer is called, controls if the outgoing net message should be sent to the server (Return false to deny)
ShouldSendLuaError(m_strError)								-				Called when there's a Lua error, controls if the Lua error should be sent to the server (Return false to deny, second return is a string that will override the error sent to the server)

------------------ Library -----------------

_G.proxi
	DEBUG													-				Value is false. Used to signal if the module is in debug mode

	FRAME_UNDEFINED											-				Value of -1. Used when FrameStageNotify hasn't ran any frames yet
	FRAME_START												-				Value of 0. Used when FrameStageNotify is about to start sending/receiving data
	FRAME_NET_UPDATE_START									-				Value of 1. Used when FrameStageNotify has received a packet
	FRAME_NET_UPDATE_POSTDATAUPDATE_START					-				Value of 2. Used when FrameStageNotify has received data is is about to call PostDataUpdate
	FRAME_NET_UPDATE_POSTDATAUPDATE_END						-				Value of 3. Used when FrameStageNotify has received data and has called PostDataUpdate on all recipients
	FRAME_NET_UPDATE_END									-				Value of 4. Used when FrameStageNotify has received all packets and interpolation, prediction, etc is about to begin
	FRAME_RENDER_START										-				Value of 5. Used when FrameStageNotify has finished sending/receiving data and is about to render the scene
	FRAME_RENDER_END										-				Value of 6. Used when FrameStageNotify has finished rendering the scene

	RunOnClient(m_strCode, m_strSource, m_bHandleError)		-				RunString on client state																										-		Returns string
	RunOnMenu(m_strCode, m_strSource, m_bHandleError)		-				RunString on menu state																											-		Returns string
	StartPrediction(CUserCmd)								-				Starts engine prediction (Only predicts on the LocalPlayer, does not predict other players)
	EndPrediction()											-				Ends engine prediction
	GetConVar()												-				Grabs ConVars without using GetConVar_Internal (Does NOT cache. Can also grab hidden ConVars such as 'cl_interpolate')			-		Returns ConVar
	GetSequenceNumber()										-				Returns your current network sequence number																					-		Returns integer
	SetSequenceNumber(m_iSequenceNumber)					-				Sets your current network sequence number (Add/Subtract from GetSequenceNumber to avoid freezing)
	GetFlowOutgoing()										-				Gets high precision latency from client to server																				-		Returns float
	GetFlowIncoming()										-				Gets high precision latency from server to client (GetFlowOutgoing + GetFlowIncoming = Highly precise ping)						-		Returns float
	SetFrameTime(m_flFrameTime)								-				Spoofs what FrameTime will return for a single call
	SetCurTime(m_flCurTime)									-				Spoofs what CurTime will return for a single call
	GetPredictionAngles()									-				Gets current prediction angles																									-		Returns Angle
	SetPredictionAngles(m_angPredictionAngle)				-				Sets your prediction angles
	GetViewAngles()											-				Returns the view angles of the screen (Binding to engine->GetViewAngles. Sets camera angles without changing CUserCmd angles)	-		Returns Angle
	SetViewAngles(m_angView)								-				Set the view angles of the screen (Binding to engine->SetViewAngles)
	DisableAnimInterp(m_bDisable)							-				Controls disabling of transitions between sequences for entities
	Lagger()												-				Voice lag exploit (Patched)
	SendVoiceData(m_strData)								-				Sends a specific voice packet
	FullUpdate()											-				Forces a full update on the client (Requests that the server resends everything)
	GetChokedCommands()										-				Returns how many commands have been choked via CreateMoveEx																		-		Returns integer
	Disconnect(m_strReason)									-				Allows disconnecting from a server with a custom reason
	SendConsoleCommand(m_strCommand)						-				Tells the server a concommand was ran on the client without actually running it on the client
	SendLuaError(m_strError)								-				Tells the server a Lua error happened on the client (Does not actually throw an error on the client)
	getinfo(m_FuncOrStackLevel, m_strFields)				-				Functions the same as debug.getinfo																								-		Returns table
	getlocal(m_pThread, m_iLevel, m_iIndex)					-				Functions the same as debug.getlocal																							-		Returns string, any
	setlocal(m_pThread, m_iLevel, m_iIndex)					-				Functions the same as debug.setlocal																							-		Returns string
	getupvalue(m_fFunction, m_iIndex)						-				Functions the same as debug.getupvalue																							-		Returns string, any
	setupvalue(m_fFunction, m_iIndex, m_Value)				-				Functions the same as debug.setupvalue																							-		Returns string
	getmetatable(m_Object)									-				Functions the same as debug.getmetatable																						-		Returns table

	net														-				Contains some proxi versions of the net library (Function exactly the same as the original, just without relying on the original implementation)
		Start												-				Same as net.Start
		SendToServer										-				Same as net.SendToServer
		NetworkIDToString									-				Same as util.NetworkIDToString																									-		Returns string
		NetworkStringToID									-				Same as util.NetworkStringToID																									-		Returns number
		WriteBit											-				Same as net.WriteBit
		WriteBool											-				Same as net.WriteBool
		WriteColor											-				Same as net.WriteColor
		WriteFloat											-				Same as net.WriteFloat
		WriteInt											-				Same as net.WriteInt
		WriteUInt											-				Same as net.WriteUInt
		WriteString											-				Same as net.WriteString
		WriteEntity											-				Same as net.WriteEntity
		WriteVector											-				Same as net.WriteVector
		WriteNormal											-				Same as net.WriteNormal
		WriteAngle											-				Same as net.WriteAngle
		WriteMatrix											-				Unimplemented

	_R														-				The registry table (Table returned by debug.getregistry)
		Entity
				GetDTNetVar									-				Gets almost any netvar on an entity (m_strValue must contain a datatable and an index such as "DT_BaseEntity->m_iHealth")		-		Returns any
					(
						Integer = 0
						Float = 1
						Vector = 2
						Vector2D = 3
						String = 4
						Entity = 5
					)
				SetDTNetVar									-				Sets almost any netvar on an entity to the given value
				SetInterpolationEnabled						-				Controls interpolation for netvars on an entity (Best used in FrameStageNotify)

		CUserCmd
				GetInWorldClicker()							-				Returns if the CUserCmd object is currently world clicking (Such as with the context menu)										-		Returns boolean
				SetInWorldClicker(m_bWorldClicker)			-				Sets if the CUserCmd object should be world clicking
				GetWorldClickerAngles()						-				Gets the current direction of the world clicker																					-		Returns Vector
				SetWorldClickerAngles(m_vecForward)			-				Sets the current direction of the world clicker
				SetCommandNumber(m_iCommandNumber)			-				Sets the CUserCmd object's ComandNumber (Useful for "changing" time)
				SetTickCount(m_iTickCoubt)					-				Sets the CUserCmd object's TickCount (Useful for "changing" time)
				HasBeenPredicted()							-				Returns if the CUserCmd object has been predicted (Seems to be unused)															-		Returns boolean
				GetIsTyping()								-				Returns if the CUserCmd has the typing flag applied																				-		Returns boolean
				SetIsTyping(m_bTyping)						-				Allows setting of the typing flag on the CUserCmd object (Useful for spoofing typing)
				GetButtonDown(m_iIndex)						-				Returns what key is being pressed at the given index (Index range of 0-4. Return values are from Enums/BUTTON_CODE)				-		Returns integer
				SetButtonDown(m_iIndex, m_iCode)			-				Allows setting what key presses will be networked to the server (Index range of 0-4)
				GetRandomSeed()								-				Returns the current random seed of the CUserCmd object (Commonly used for bullet spread. Range of 0-255)						-		Returns integer
				SetRandomSeed(m_iSeed)						-				Allows setting the random seed of the CUserCmd object (Commonly used for nospread. Range of 0-255)

		ConVar
				ForceFloat(m_flValue)						-				Forces the float value of the convar to the given value
				ForceBool(m_bValue)							-				Forces the boolean value of the convar to the given value
				ForceInt(m_iValue)							-				Forces the integer value of the convar to the given value
				ForceString(m_strValue)						-				Forces the string value of the convar to the given value
				ForceMax(m_flValue)							-				Forces the maximum value of the convar to the given value
				ForceMin(m_flValue)							-				Forces the minimum value of the convar to the given value
				ForceHasMax(m_bHasMax)						-				Forces if the ConVar has an applied maximum value
				ForceHasMin(m_bHasMin)						-				Forces if the ConVar has an applied minimum value
				SetFlags(m_iFlags)							-				Sets the flags on the ConVar (Enums/FCVAR)
				SendValue(m_Value)							-				Tells the server a ConVar was set to a value without actually setting it on client

------------------ Classes -----------------

proxi_bf_read												-				Object passed through the 'OutgoingNetMessage' hook
	GetBitsLeft												-				Returns how many bits are left																									-		Returns integer
	GetBytesLeft											-				Returns how many bytes are left																									-		Returns integer
	GetBitsRead												-				Returns how many bits have been read																							-		Returns integer
	GetBytesRead											-				Returns how many bytes have been read																							-		Returns integer
	GetCurBit												-				Returns the current bit index																									-		Returns integer
	ReadByte												-				Returns the next byte																											-		Returns 8-bit integer
	ReadWord												-				Returns the next word																											-		Returns 16-bit integer
	ReadAngle												-				Returns the next Angle																											-		Returns Angle
	ReadBit													-				Returns the next bit																											-		Returns integer
	ReadBool												-				Returns the next bool																											-		Returns boolean
	ReadColor(m_bHasAlpha)									-				Returns the next color, m_bHasAlpha will attempt to read the alpha of the color													-		Returns Color
	ReadData												-				Returns the next chunk of data																									-		Returns string
	ReadDouble												-				Returns the next double																											-		Returns float
	ReadEntity												-				Returns the next Entity																											-		Returns Entity
	ReadFloat												-				Returns the next float																											-		Returns float
	ReadInt(m_iBitCount)									-				Returns the next integer of the given size																						-		Returns integer
	ReadMatrix												-				Unimplemented																													-		Returns VMatrix
	ReadNormal												-				Returns the next normal Vector																									-		Returns Vector
	ReadString												-				Returns the next string																											-		Returns string
	ReadUInt(m_iBitCount)									-				Returns the next unsigned integer of the given size																				-		Returns integer
	ReadVector												-				Returns the next Vector																											-		Returns Vector
