--[[
	Creates a table that can be used instead of player.GetAll to avoid extra function calls and new table creations
]]

local PLAYER_LIST = {} -- Cached player list that will be used instead of player.GetAll
local WAITING_CONNECTIONS = {} -- Used in case a player is missed when the connect event is fired to grab them when they become valid

local function PLAYER_SORTER(a, b) -- Sort players in the same order as player.GetAll
	return a:EntIndex() < b:EntIndex()
end

local function AddPlayer(pPlayer) -- Adds a player and re-sorts the entire list to make sure it's in the right order
	PLAYER_LIST[#PLAYER_LIST + 1] = pPlayer
	table.sort(PLAYER_LIST, PLAYER_SORTER)
end

local function RemoveBadPlayers() -- Removes all NULL players
	for i = #PLAYER_LIST, 1, -1 do
		local pPlayer = PLAYER_LIST[i]

		if not IsValid(pPlayer) or not pPlayer:IsPlayer() then
			table.remove(PLAYER_LIST, i)
		end
	end
end

local function AddMissingPlayers() -- Adds players that somehow got missed (Lag, anticheats, initial script load)
	if #PLAYER_LIST < 1 then -- Not initialized yet
		PLAYER_LIST = player.GetAll()
		return
	end

	local contained = {}

	for i = 1, #PLAYER_LIST do -- Convert values into keys
		contained[PLAYER_LIST[i]] = true
	end

	for _, v in ipairs(player.GetAll()) do -- Add any missing UserIDs
		if not contained[v] then
			AddPlayer(v)
		end
	end
end

local function AuthPlayer(pPlayer) -- Add a player that wasn't valid in the player_connect_client event (Likely didn't spawn in when they were supposed to)
	if WAITING_CONNECTIONS[pPlayer:SteamID()] then
		AddPlayer(pPlayer)
		WAITING_CONNECTIONS[pPlayer:SteamID()] = nil
	end
end
hook.Add("PlayerTick", "AuthPlayer", AuthPlayer)
hook.Add("VehicleMove", "AuthPlayer", AuthPlayer) -- Awesome video game

gameevent.Listen("player_connect_client")
hook.Add("player_connect_client", "AddPlayer", function(data)
	if not istable(data) then return end -- Make sure we're working with something valid
	if not isnumber(data.index) or not isnumber(data.userid) then return end
	if not isstring(data.networkid) then return end

	local entity = Entity(data.index + 1) or Player(data.userid)

	if IsValid(entity) and entity:IsPlayer() then
		AddPlayer(entity)
	else
		if data.networkid ~= "BOT" then -- Bots will be have to be caught by the redundancy in this case
			WAITING_CONNECTIONS[data.networkid] = true
		end
	end
end)

gameevent.Listen("player_disconnect")
hook.Add("player_disconnect", "RemovePlayer", function(data) -- The player becomes NULL when this event is called, grabbing any information about them is impossible here
	if not istable(data) then return end

	RemoveBadPlayers()
end)

hook.Add("Tick", "PlayerRedundancy", function() -- Account for any skips or hiccups every now and then by comparing the amount of players in the cache to what it should be
	if player.GetCount() ~= #PLAYER_LIST then
		RemoveBadPlayers()
		AddMissingPlayers()
	end
end)

do
	AddMissingPlayers() -- Initialize the list with players
end

---------------------------------------------------------

timer.Create("test timer", 1, 0, function() -- Debug to compare
	print("----------------------------------------------------------------------------------------------")
	print("---------------   player.GetAll   ---------------")
	PrintTable(player.GetAll())
	print("-------------------------------------------------")
	print("----------------   PLAYER_LIST   ----------------")
	PrintTable(PLAYER_LIST)
	print("----------------------------------------------------------------------------------------------")
end)
